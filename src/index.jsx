import React from "react";
import { BrowserRouter } from "react-router-dom";
import App from "./Components/App";
// import { createRoot } from "react-dom/client";
import "./index.css";
import ReactDOM from "react-dom/client";

// const root = createRoot(document.getElementById("root")); 
// root.render(
 
// );

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <BrowserRouter>
    <App />
  </BrowserRouter>
);
