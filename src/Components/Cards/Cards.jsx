import { Component } from "react";
import { Button, Card, CardContent, Typography } from "@material-ui/core";
import AddIcon from "@mui/icons-material/Add";
import TextField from "@mui/material/TextField";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import DeleteOutlineRoundedIcon from "@mui/icons-material/DeleteOutlineRounded";
import axios from "axios";
import CloseRoundedIcon from "@mui/icons-material/CloseRounded";
import Checklist from "../CheckList/Checklist";

class Cards extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cards: [],
      isCardOpen: false,
      cardName: "",
      cardID: "",
      isCheckListOpen: false,
    };
  }

  handleOpen = () => {
    this.setState({
      isCardOpen: true,
    });
  };
  handleClose = () => {
    this.setState({
      isCardOpen: false,
    });
  };

  inputText = (event) => {
    this.setState({
      cardName: event.target.value,
    });
  };

  CheckListDialogOpen = () => {
    this.setState({
      isCheckListOpen: true,
    });
  };

  CheckListDialogClose = ()=>{
     this.setState({
      isCheckListOpen: false,
    });
  }

  cardApiStore = () => {
    let APIToken =
      "ATTA7875ab43dab33543d7ab7fe5343a26ff3a24bb1ea95eb2bb14df2173b09ce5533FB6F3FE";
    let APIKey = "d1159a38fed10881ba6c9a948a196ac4";
    axios
      .post(
        `https://api.trello.com/1/cards?idList=${this.props.id}&name=${this.state.cardName}&key=${APIKey}&token=${APIToken}`
      )
      .then((response) => {
        this.setState({
          cards: [...this.state.cards, response.data],
        });
      })
      .catch((err) => console.error(err));
  };

  createCard = () => {
    this.handleClose();
    this.cardApiStore();
  };

  deleteCard = (id) => {
    let APIToken =
      "ATTA7875ab43dab33543d7ab7fe5343a26ff3a24bb1ea95eb2bb14df2173b09ce5533FB6F3FE";
    let APIKey = "d1159a38fed10881ba6c9a948a196ac4";
    axios
      .delete(
        `https://api.trello.com/1/cards/${id}?key=${APIKey}&token=${APIToken}`
      )
      .then((res) => {
        let updatedCard = this.state.cards.filter((item) => {
          return item.id !== id;
        });
        this.setState({ cards: updatedCard });
      });
  };

  componentDidMount() {
    let APIToken =
      "ATTA7875ab43dab33543d7ab7fe5343a26ff3a24bb1ea95eb2bb14df2173b09ce5533FB6F3FE";
    let APIKey = "d1159a38fed10881ba6c9a948a196ac4";
    axios
      .get(
        `https://api.trello.com/1/lists/${this.props.id}/cards?key=${APIKey}&token=${APIToken}`
      )
      .then((responce) => {
        this.setState({
          cards: responce.data,
        });
      })
      .catch((error) => {
        console.log(error);
      });
  }

  render() {
    const { isCardOpen, isCheckListOpen } = this.state;
    return (
      <>
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            padding: "1%",
            rowGap: "0.5rem",
          }}
        >
          {this.state.cards.map((item) => {
            return (
              <Card>
                <CardContent
                  style={{
                    display: "flex",
                    justifyContent: "space-between",
                  }}
                >
                  <Typography
                    onClick={(id) => {
                      this.setState({
                        cardID: item.id,
                        cardName: item.name,
                      });
                      this.CheckListDialogOpen();
                    }}
                  >
                    {item.name}
                  </Typography>
                  <DeleteOutlineRoundedIcon
                    onClick={(id) => {
                      this.deleteCard(item.id);
                    }}
                  ></DeleteOutlineRoundedIcon>
                </CardContent>
              </Card>
            );
          })}
        </div>
        {isCheckListOpen && (
          <div>
            <Dialog open={isCheckListOpen} onClose={this.handleClose}>
              <span>Checklist Name</span>
              <DialogTitle
                style={{ display: "flex", justifyContent: "space-between" }}
              >
                {this.state.cardName}
                <CloseRoundedIcon
                  color="primary"
                  onClick={this.CheckListDialogClose}
                ></CloseRoundedIcon>
              </DialogTitle>

              <Checklist cardId={this.state.cardID}></Checklist>
            </Dialog>
          </div>
        )}
        {isCardOpen && (
          <div>
            <Dialog open={isCardOpen} onClose={this.handleClose}>
              <DialogTitle>Add New Card</DialogTitle>
              <DialogContent>
                <TextField
                  autoFocus
                  margin="dense"
                  id="name"
                  label="Card Name"
                  type="text"
                  fullWidth
                  variant="standard"
                  autoComplete="off"
                  onChange={this.inputText}
                />
              </DialogContent>
              <DialogActions>
                <Button onClick={this.handleClose}>Cancel</Button>
                <Button onClick={this.createCard}>Create Card</Button>
              </DialogActions>
            </Dialog>
          </div>
        )}
        <Button onClick={this.handleOpen} startIcon={<AddIcon />}>
          Add a card
        </Button>
      </>
    );
  }
}

export default Cards;
