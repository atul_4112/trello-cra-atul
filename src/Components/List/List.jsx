import axios from "axios";
import { Component } from "react";
import { Button, Card, CardContent, Typography } from "@material-ui/core";
import DeleteIcon from "@mui/icons-material/Delete";
import AddCircleOutlineOutlinedIcon from "@mui/icons-material/AddCircleOutlineOutlined";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import TextField from "@mui/material/TextField";
import Cards from "../Cards/Cards";

class List extends Component {
  constructor(props) {
    super(props);
    this.state = {
      list: [],
      listName: "",
      isListOpen: false,
    };
  }

  inputText = (event) => {
    this.setState({
      listName: event.target.value,
    });
  };

  handleOpen = () => {
    this.setState({
      isListOpen: true,
    });
  };
  handleClose = () => {
    this.setState({
      isListOpen: false,
    });
  };

  listApiStore = () => {
    const { id } = this.props.match.params;
    let APIToken =
      "ATTA7875ab43dab33543d7ab7fe5343a26ff3a24bb1ea95eb2bb14df2173b09ce5533FB6F3FE";
    let APIKey = "d1159a38fed10881ba6c9a948a196ac4";
    axios
      .post(
        `https://api.trello.com/1/lists?name=${this.state.listName}&idBoard=${id}&key=${APIKey}&token=${APIToken}`
      )
      .then((response) => {
        this.setState({
          list: [...this.state.list, response.data],
          open: false,
        });
      })
      .catch((err) => console.error(err));
  };

  createList = () => {
    this.handleClose();
    this.listApiStore();
  };

  deleteList = (id) => {
    let APIToken =
      "ATTA7875ab43dab33543d7ab7fe5343a26ff3a24bb1ea95eb2bb14df2173b09ce5533FB6F3FE";
    let APIKey = "d1159a38fed10881ba6c9a948a196ac4";
    axios
      .put(
        `https://api.trello.com/1/lists/${id}/closed?value=true&key=${APIKey}&token=${APIToken}`
      )
      .then((res) => {
        let updatedList = this.state.list.filter((item) => {
          return item.id !== id;
        });
        this.setState({ list: updatedList });
      });
  };

  componentDidMount() {
    const { id } = this.props.match.params;
    let APIToken =
      "ATTA7875ab43dab33543d7ab7fe5343a26ff3a24bb1ea95eb2bb14df2173b09ce5533FB6F3FE";
    let APIKey = "d1159a38fed10881ba6c9a948a196ac4";
    axios
      .get(
        `https://api.trello.com/1/boards/${id}/lists?key=${APIKey}&token=${APIToken}`
      )
      .then((responce) => {
        this.setState({
          list: responce.data,
        });
      })
      .catch((error) => {
        console.log(error);
      });
  }
  render() {
    const { isListOpen } = this.state;
    return (
      <div
        style={{
        //   background: "#07126b",
          display: "flex",
          padding: "1%",
          rowGap: "1rem",
          columnGap: "1rem",
          height: "maxHeight",
          flexWrap: "wrap",
          width: "100%",
        }}
      >
        {this.state.list.map((item) => {
          return (
            <Card style={{ width: "20%" }}>
              <CardContent>
                <Typography
                  style={{
                    display: "flex",
                    justifyContent: "space-between",
                  }}
                >
                  {item.name}
                  <DeleteIcon
                    onClick={(id) => {
                      this.deleteList(item.id);
                    }}
                  ></DeleteIcon>
                </Typography>
                <Cards id = {item.id}></Cards>

              </CardContent>
            </Card>
          );
        })}
        <Button
          variant="outlined"
          onClick={this.handleOpen}
          startIcon={<AddCircleOutlineOutlinedIcon />}
          style={{ height: "3rem", color: "wheat" }}
        >
          Add Another List
        </Button>
        {isListOpen && (
          <div>
            <Dialog open={isListOpen} onClose={this.handleClose}>
              <DialogTitle>Add New List</DialogTitle>
              <DialogContent>
                <TextField
                  autoFocus
                  margin="dense"
                  id="name"
                  label="List Name"
                  type="text"
                  fullWidth
                  variant="standard"
                  autoComplete="off"
                  onChange={this.inputText}
                />
              </DialogContent>
              <DialogActions>
                <Button onClick={this.handleClose}>Cancel</Button>
                <Button onClick={this.createList}>Create</Button>
              </DialogActions>
            </Dialog>
          </div>
        )}
      </div>
    );
  }
}

export default List;