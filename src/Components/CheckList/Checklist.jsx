import { Component } from "react";
import { Button, Card, CardContent, Typography } from "@material-ui/core";
import DeleteIcon from "@mui/icons-material/Delete";
import TextField from "@mui/material/TextField";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import axios from "axios";
import CheckItem from "../CheckItem/CheckItem";

class Checklist extends Component {
  constructor(props) {
    super(props);
    this.state = {
      checklist: [],
      checkListName: "",
      addCheckListDialog: false,
    };
  }

  inputText = (event) => {
    this.setState({
      checkListName: event.target.value,
    });
  };

  handleOpen = () => {
    this.setState({
      addCheckListDialog: true,
    });
  };

  handleClose = () => {
    this.setState({
      addCheckListDialog: false,
    });
  };

  checkListApiStore = () => {
    let APIToken =
      "ATTA7875ab43dab33543d7ab7fe5343a26ff3a24bb1ea95eb2bb14df2173b09ce5533FB6F3FE";
    let APIKey = "d1159a38fed10881ba6c9a948a196ac4";
    axios
      .post(
        `https://api.trello.com/1/checklists?name=${this.state.checkListName}&idCard=${this.props.cardId}&key=${APIKey}&token=${APIToken}`
      )
      .then((response) => {
        this.setState({
          checklist: [...this.state.checklist, response.data],
        });
      })
      .catch((err) => console.error(err));
  };

  createCheckList = () => {
    this.handleClose();
    this.checkListApiStore();
  };

  componentDidMount() {
    let APIToken =
      "ATTA7875ab43dab33543d7ab7fe5343a26ff3a24bb1ea95eb2bb14df2173b09ce5533FB6F3FE";
    let APIKey = "d1159a38fed10881ba6c9a948a196ac4";
    axios
      .get(
        `https://api.trello.com/1/cards/${this.props.cardId}/checklists?key=${APIKey}&token=${APIToken}`
      )
      .then((responce) => {
        this.setState({
          checklist: responce.data,
        });
      })
      .catch((error) => {
        console.log(error);
      });
  }

  deleteCheckList = (id) => {
    let APIToken =
      "ATTA7875ab43dab33543d7ab7fe5343a26ff3a24bb1ea95eb2bb14df2173b09ce5533FB6F3FE";
    let APIKey = "d1159a38fed10881ba6c9a948a196ac4";
    axios
      .delete(
        `https://api.trello.com/1/checklists/${id}?key=${APIKey}&token=${APIToken}`
      )
      .then((res) => {
        let updatedList = this.state.checklist.filter((item) => {
          return item.id !== id;
        });
        this.setState({ checklist: updatedList });
      });
  };

  render() {
    const { addCheckListDialog } = this.state;
    return (
      <>
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            padding: "1%",
            rowGap: "1rem",
            flexWrap: "wrap",
            width: "100%",
          }}
        >
          {this.state.checklist.map((item) => {
            return (
              <Card>
                <CardContent>
                  <Typography
                    style={{
                      display: "flex",
                      justifyContent: "space-between",
                    }}
                  >
                    {item.name}
                    <DeleteIcon
                      onClick={(id) => {
                        this.deleteCheckList(item.id);
                      }}
                    ></DeleteIcon>
                  </Typography>
                </CardContent>
                <DialogActions>
                    <CheckItem checkName ={item.name} id={item.id}> </CheckItem>
                </DialogActions>
              </Card>
            );
          })}
        </div>
        <Button variant="contained" color="primary" onClick={this.handleOpen}>
          Create Checklist
        </Button>

        {addCheckListDialog && (
          <div>
            <Dialog open={addCheckListDialog} onClose={this.handleClose}>
              <DialogTitle>Add New CheckList</DialogTitle>
              <DialogContent>
                <TextField
                  autoFocus
                  margin="dense"
                  id="name"
                  label="CheckList Name"
                  type="text"
                  fullWidth
                  variant="standard"
                  autoComplete="off"
                  onChange={this.inputText}
                />
              </DialogContent>
              <DialogActions>
                <Button onClick={this.handleClose}>Cancel</Button>
                <Button onClick={this.createCheckList}>Create CheckList</Button>
              </DialogActions>
            </Dialog>
          </div>
        )}
      </>
    );
  }
}

export default Checklist;
