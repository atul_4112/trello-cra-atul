import { React, Component } from "react";
import { Container } from "@material-ui/core";
import { Card, CardContent, Typography, Button } from "@material-ui/core";
import TextField from "@mui/material/TextField";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import axios from "axios";
import Test from "./Test";
class Board extends Component {
  constructor(props) {
    super(props);
    this.state = { isOpen: false, openCards: [], boardName: "" };
  }

  handleOpen = () => {
    this.setState({
      isOpen: true,
    });
  };
  handleClose = () => {
    this.setState({
      isOpen: false,
    });
  };

  inputText = (event) => {
    this.setState({
      boardName: event.target.value,
    });
  };

  boardApiStore = () => {
    let APIToken =
      "ATTA7875ab43dab33543d7ab7fe5343a26ff3a24bb1ea95eb2bb14df2173b09ce5533FB6F3FE";
    let APIKey = "d1159a38fed10881ba6c9a948a196ac4";
    axios
      .post(
        `https://api.trello.com/1/boards/?name=${this.state.boardName}&key=${APIKey}&token=${APIToken}`
      )
      .then((response) => {
        this.setState({
          openCards: [...this.state.openCards, response.data],
          open: false,
        });
      })
      .catch((err) => console.error(err));
  };

  createBoard = () => {
    this.handleClose();
    this.boardApiStore();
  };

  componentDidMount() {
    let APIToken =
      "ATTA7875ab43dab33543d7ab7fe5343a26ff3a24bb1ea95eb2bb14df2173b09ce5533FB6F3FE";
    let APIKey = "d1159a38fed10881ba6c9a948a196ac4";
    axios
      .get(
        `https://api.trello.com/1/members/me/boards?key=${APIKey}&token=${APIToken}`
      )

      .then((data) =>
        this.setState({
          openCards: data.data,
        })
      )
      .catch((err) =>
        this.setState({
          openCards: [],
        })
      );
  }
  render() {
    const { isOpen } = this.state;
    return (
      <>
        <Container maxWidth="md" style={{ marginTop: "1%" }}>
          <span style={{ fontSize: "1.2rem", color: "Wheat" }}>
            YOUR WORKSPACE
          </span>
          <br />
          <div
            style={{
              display: "flex",
              fontSize: "1.2rem",
              gap: "1%",
              color: "Wheat",
            }}
          >
            <img
              src="https://img.icons8.com/external-others-inmotus-design/512/external-T-keyboard-others-inmotus-design-4.png"
              width="3%"
              alt="img"
            />
            Trello Workspace
          </div>

          <div style={{ display: "flex", flexWrap: "wrap", columnGap: "1rem" }}>
            <Test props={this.state.openCards} />
            <Card
              onClick={this.handleOpen}
              style={{
                marginTop: "1%",
                width: "25%",
                height: "8rem",
                background: "wheat",
                color: "Black",
                borderRadius: "0.5rem",
                cursor: "pointer",
              }}
            >
              <CardContent>
                <Typography variant="h6">Create new Board</Typography>
              </CardContent>
            </Card>
          </div>
        </Container>

        {isOpen && (
          <div>
            <Dialog open={isOpen} onClose={this.handleClose}>
              <DialogTitle>Create Board</DialogTitle>
              <DialogContent>
                <TextField
                  autoFocus
                  margin="dense"
                  id="name"
                  label="Board Name"
                  type="text"
                  fullWidth
                  variant="standard"
                  autoComplete="off"
                  onChange={this.inputText}
                />
              </DialogContent>
              <DialogContentText ml={2}>
                👋 Board title is required
              </DialogContentText>
              <DialogActions>
                <Button onClick={this.handleClose}>Cancel</Button>
                <Button onClick={this.createBoard}>Create</Button>
              </DialogActions>
            </Dialog>
          </div>
        )}
      </>
    );
  }
}

export default Board;
