import React, { Component } from "react";
import { Card, CardContent, Typography } from "@material-ui/core";
import { Link } from "react-router-dom";

export default class Test extends Component {
  render() {
    return (
      <>
        {this.props.props
          ? this.props.props.map((board) => {
              return (
                <Card
                  key={board.id}
                  style={{
                    marginTop: "1%",
                    width: "25%",
                    height: "8rem",
                    background: "#3e54a3",
                    color: "wheat",
                    borderRadius: "0.5rem",
                  }}
                >
                  <Link to={`${board.id}`} style={{textDecoration:'none' , color:'wheat'}}>
                    <CardContent>
                      {board ? (
                        <Typography variant="h6">{board.name}</Typography>
                      ) : null}
                    </CardContent>
                  </Link>
                </Card>
              );
            })
          : null}
      </>
    );
  }
}
