import { Component} from 'react'
import Board from './Board/Board';
import Header from './Header/Header';
import { Switch, Route } from "react-router-dom";
import List from './List/List';

class App extends Component{
  render(){
    return (
      <>
        <Header />
        <Switch>
          <Route exact path="/" component={Board} />
          <Route exact path="/:id" component={List} />
        </Switch>

        {/* <Board /> */}
      </>
    );
  }
}

export default App
