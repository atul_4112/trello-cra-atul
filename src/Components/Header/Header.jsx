import { Component } from "react";
import { AppBar, Toolbar, Typography } from "@material-ui/core";
import { Link } from "react-router-dom";

class Header extends Component{
    render(){
        return (
          <AppBar position="static">
            <Toolbar>
              <img
                style={{ width: "8rem" }}
                src={`https://a.trellocdn.com/prgb/dist/images/header-logo-spirit-loading.87e1af770a49ce8e84e3.gif`}
                loading="lazy"
                alt='mi'
              />
              <Link to='/' style={{textDecoration:"none" ,color:'White'}}>
                <Typography style={{ marginLeft: "4rem" }} variant="h5">
                  Home
                </Typography>
              </Link>
            </Toolbar>
          </AppBar>
        );
    }
}

export default Header;