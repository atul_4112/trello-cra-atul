import { Component } from "react";
import { Button, Card, CardContent, Typography } from "@material-ui/core";
import DeleteIcon from "@mui/icons-material/Delete";
import TextField from "@mui/material/TextField";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import { Checkbox, FormControlLabel } from "@material-ui/core";
import axios from "axios";

class CheckItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items: [],
      itemName: "",
      addItemDialog: false,
      isChecked: false,
      checkBox: [{isChecked: false }],
    };
  }

  handleCheckboxChange = (event, id) => {
    const newCheckBox = this.state.checkBox.map((checkbox) => {
      if (checkbox.id === id) {
        checkbox.isChecked = event.target.checked;
      }
      return checkbox;
    });
    this.setState({
      checkBox: newCheckBox,
    });
  };

  inputText = (event) => {
    this.setState({
      itemName: event.target.value,
    });
  };

  handleOpen = () => {
    this.setState({
      addItemDialog: true,
    });
  };

  handleClose = () => {
    this.setState({
      addItemDialog: false,
    });
  };

  itemApiStore = () => {
    let APIToken =
      "ATTA7875ab43dab33543d7ab7fe5343a26ff3a24bb1ea95eb2bb14df2173b09ce5533FB6F3FE";
    let APIKey = "d1159a38fed10881ba6c9a948a196ac4";
    axios
      .post(
        `https://api.trello.com/1/checklists/${this.props.id}/checkItems?name=${this.state.itemName}&key=${APIKey}&token=${APIToken}`
      )
      .then((response) => {
        this.setState({
          items: [...this.state.items, response.data],
          checkBox: [...this.state.checkBox, response.data],
        });
      })
      .catch((err) => console.error(err));
  };

  createItem = () => {
    this.handleClose();
    this.itemApiStore();
  };

  componentDidMount() {
    let APIToken =
      "ATTA7875ab43dab33543d7ab7fe5343a26ff3a24bb1ea95eb2bb14df2173b09ce5533FB6F3FE";
    let APIKey = "d1159a38fed10881ba6c9a948a196ac4";
    axios
      .get(
        `https://api.trello.com/1/checklists/${this.props.id}/checkItems?key=${APIKey}&token=${APIToken}`
      )
      .then((responce) => {
        this.setState({
          items: responce.data,
          checkBox: responce.data,
        });
      })
      .catch((error) => {
        console.log(error);
      });
  }

  deleteCheckList = (id) => {
    let APIToken =
      "ATTA7875ab43dab33543d7ab7fe5343a26ff3a24bb1ea95eb2bb14df2173b09ce5533FB6F3FE";
    let APIKey = "d1159a38fed10881ba6c9a948a196ac4";
    axios
      .delete(
        `https://api.trello.com/1/checklists/${this.props.id}/checkItems/${id}?key=${APIKey}&token=${APIToken}`
      )
      .then((res) => {
        let updatedItems = this.state.items.filter((item) => {
          return item.id !== id;
        });
        this.setState({ items: updatedItems });
      });
  };
  render() {
    const { addItemDialog } = this.state;
    return (
      <>
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            padding: "1%",
            rowGap: "1rem",
            flexWrap: "wrap",
            width: "100%",
          }}
        >
          {this.state.items.map((item) => {
            return (
              <Card>
                <CardContent>
                  <Typography
                    style={{
                      display: "flex",
                      justifyContent: "space-between",
                    }}
                  >
                    <FormControlLabel
                      control={
                        <Checkbox
                          checked={this.state.checkBox.isChecked}
                          onChange={(event) =>
                            this.handleCheckboxChange(event, item.id)
                          }
                        />
                      }
                      label={
                        <Typography
                          variant="body1"
                          className={
                            item.isChecked ? "underline" : null
                          }
                        >
                          {item.name}
                        </Typography>
                      }
                    />
                    <DeleteIcon
                      onClick={(id) => {
                        this.deleteCheckList(item.id);
                      }}
                    ></DeleteIcon>
                  </Typography>
                </CardContent>
                <DialogActions></DialogActions>
              </Card>
            );
          })}
          <Button onClick={this.handleOpen}>Create Item</Button>
        </div>
        {addItemDialog && (
          <div>
            <Dialog open={addItemDialog} onClose={this.handleClose}>
              <DialogTitle>Add New Item</DialogTitle>
              <DialogContent>
                <TextField
                  autoFocus
                  margin="dense"
                  id="name"
                  label="Item Name"
                  type="text"
                  fullWidth
                  variant="standard"
                  autoComplete="off"
                  onChange={this.inputText}
                />
              </DialogContent>
              <DialogActions>
                <Button onClick={this.handleClose}>Cancel</Button>
                <Button onClick={this.createItem}>Create CheckList</Button>
              </DialogActions>
            </Dialog>
          </div>
        )}
      </>
    );
  }
}

export default CheckItem;
